const { ipcMain } = require("electron")
const SerialPort = require("serialport")
const ReadLine = require("@serialport/parser-readline")

let conection = false
let response = []

ipcMain.handle("serial.list", async(event, arg) => {
  const port = await SerialPort.list()
  return port
})

ipcMain.handle("serial.read", (event, { port }) => {
  if (!conection) {
    conection = new SerialPort(port, { baudRate: 115200 })
    const parser = conection.pipe(new ReadLine({ delimiter: "\r\n" }))
    parser.on("data", (data) => {
      response.push(data)
    })
  }
  return response
})

ipcMain.handle("serial.close", (event) => {
  // console.log({ port })
  if (conection) {
    conection.flush()
    conection.close()
    conection = false
    response = []
  }
  return true
})
